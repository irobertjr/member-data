<?php
Class Insert_data extends CI_Controller {	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('minsert');
		$this->load->helper(array('url','form'));
	}
	function index() {
		$data['hasil'] = $this->minsert->getall();
		$this->load->view('v_insert', $data);
	}
	
	function tambahdata() {
		if($this->input->post('daftar')){
			$this->minsert->tambah();
		}
		$this->load->view('v_insert');
	}
}
?>