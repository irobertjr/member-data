<?php
class Minsert extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	} 
    function getall() {
        $ambildata = $this->db->get('member');
        //jika data ada (lebih dari 0)
        if ($ambildata->num_rows() > 0 ) {
            foreach ($ambildata->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
 
    function tambah() {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $data = array (
            'member_fname' => $fname,
            'member_lname'  => $lname,
            'member_login'=> $username,
            'member_password' => $password
        );  
        $data_memberaccount = array (
            'member_login'=> $username,
            'account_password' => $password
        );  
        if ($this->db->insert('member',$data)) {
            echo "Berhasil!";
        } else {
            echo "Gagal!";
        }

    }
}
?>